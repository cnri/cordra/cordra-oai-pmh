package net.cnri.cordra.oaipmh;

import net.cnri.cordra.api.CordraClient;
import net.cnri.cordra.api.CordraException;
import net.cnri.cordra.api.CordraObject;

public interface MetadataGenerator {

    String generateMetadataFrom(CordraObject co, CordraClient cordra) throws CordraException;
}
