package net.cnri.cordra.oaipmh;

import java.util.ArrayList;
import java.util.List;

public class OaiPmhConfig {
    public OaiPmhIdentity identity;
    public String cordraBaseUri = "http://localhost:8081/cordra";
    public int maxPageSize = 1000;
    public List<String> cordraTypes;

    public OaiPmhConfig() {
        cordraTypes = new ArrayList<>();
    }
}
