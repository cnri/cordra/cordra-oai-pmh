package net.cnri.cordra.oaipmh;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.cnri.cordra.api.CordraClient;
import net.cnri.cordra.api.CordraException;
import net.cnri.cordra.api.CordraObject;
import net.cnri.cordra.api.Options;
import net.cnri.util.StringUtils;

public class DublinCoreMetadataGenerator implements MetadataGenerator {

    private static String[] properties = {"title", "creator", "subject", "description", "publisher",
            "contributor", "date", "type", "format", "identifier", "source", "language", "relation", "coverage", "rights"};

    public DublinCoreMetadataGenerator() {

    }

    @Override
    public String generateMetadataFrom(CordraObject co, CordraClient cordra) throws CordraException {
        Options options = new Options().setUseDefaultCredentials(true);
        JsonObject params = new JsonObject();
        JsonObject dcJson = cordra.call(co.id, "getAsDublinCoreJson", params, options).getAsJsonObject();
        String dcMetadata = jsonToOaiDcMetadataXml(dcJson);
        return dcMetadata;
    }

    private static String xmlForName(JsonObject obj, String name) {
        String xml = "";
        String pluralName = name + "s";
        if (obj.has(name)) {
            JsonElement el = obj.get(name);
            if (el.isJsonArray()) {
                JsonArray array = el.getAsJsonArray();
                xml = xmlForArray(name, array);
            } else {
                String value = obj.get(name).getAsString();
                xml = xmlElementFor(name, value);
            }
        } else if (obj.has(pluralName)) {
            JsonElement el = obj.get(pluralName);
            if (el.isJsonArray()) {
                JsonArray array = el.getAsJsonArray();
                xml = xmlForArray(name, array);
            }
        }
        return xml;
    }

    private static String xmlForArray(String name, JsonArray array) {
        StringBuilder xml = new StringBuilder();
        for (int i = 0; i < array.size(); i++) {
            String value = array.get(i).getAsString();
            xml.append(xmlElementFor(name, value));
        }
        return xml.toString();
    }

    private static String xmlElementFor(String name, String value) {
        return "<dc:" + name + ">" + StringUtils.xmlEscape(value) + "</dc:" + name + ">";
    }

    public static String jsonToOaiDcMetadataXml(JsonObject metadata) {
        StringBuilder xml = new StringBuilder();
        xml.append("<metadata>");
        xml.append("<oai_dc:dc xmlns:oai_dc=\"http://www.openarchives.org/OAI/2.0/oai_dc/\" xmlns:dc=\"http://purl.org/dc/elements/1.1/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.openarchives.org/OAI/2.0/oai_dc/ http://www.openarchives.org/OAI/2.0/oai_dc.xsd\">");
        for (String property : properties) {
            String xmlForName = xmlForName(metadata, property);
            if (!"".equals(xmlForName)) {
                xml.append(xmlForName);
            }
        }
        xml.append("</oai_dc:dc>");
        xml.append("</metadata>");
        return xml.toString();
    }
}
