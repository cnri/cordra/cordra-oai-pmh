package net.cnri.cordra.oaipmh;

import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@WebServlet({"/"})
public class OaiPmhServlet extends HttpServlet {

    private static Logger logger = LoggerFactory.getLogger(OaiPmhServlet.class);

    private CordraOaiPmhService oaiPmhService;

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        OaiPmhConfig oaiPmhConfig;
        try {
            oaiPmhConfig = loadConfig();
            if (oaiPmhConfig == null) {
                System.out.println("Error: Failed to load oai-pmh-config.json");
            }
        } catch (IOException e) {
            throw new ServletException(e);
        }
        this.oaiPmhService = new CordraOaiPmhService(oaiPmhConfig);
    }

    private OaiPmhConfig loadConfig() throws IOException {
        String dataDir = System.getProperty("cordra.data");
        if (dataDir == null) dataDir = System.getenv("cordra.data");
        if (dataDir == null) dataDir = System.getProperty("cordra_data");
        if (dataDir == null) dataDir = System.getenv("cordra_data");
        if (dataDir != null) {
            Path path = Paths.get(dataDir).resolve("oai-pmh-config.json");
            if (Files.exists(path)) {
                try (Reader in = Files.newBufferedReader(path)) {
                    return new Gson().fromJson(in, OaiPmhConfig.class);
                }
            }
        }
        return null;
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        handleRequest(request, response);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        handleRequest(request, response);
    }

    private void handleRequest(HttpServletRequest request, HttpServletResponse response) throws IOException {
        OaiPmhRequest oaiPmhRequest = new OaiPmhRequest(request);
        try {
            String responseXml = oaiPmhService.processRequest(oaiPmhRequest);
            Writer out = response.getWriter();
            out.write(responseXml);
            out.close();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }
    }
}
