package net.cnri.cordra.oaipmh;

import com.google.gson.Gson;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

public class ResumptionToken {

    private static Gson gson = new Gson();

    public Long txnId = 0L;
    public String from;
    public String until;
    public String set;
    public String metadataPrefix;

    public static String toToken(Long txnId, String from, String until, String set, String metadataPrefix) {
        ResumptionToken tokenObject = new ResumptionToken();
        tokenObject.from = from;
        tokenObject.until = until;
        tokenObject.txnId = txnId;
        tokenObject.metadataPrefix = metadataPrefix;
        String json = gson.toJson(tokenObject);
        String resumptionToken = Base64.getUrlEncoder().withoutPadding().encodeToString(json.getBytes());
        return resumptionToken;
    }

    public static ResumptionToken fromToken(String resumptionToken) {
        try {
            byte[] decodedBytes = Base64.getUrlDecoder().decode(resumptionToken.getBytes());
            String json = new String(decodedBytes, StandardCharsets.UTF_8);
            ResumptionToken tokenObject = gson.fromJson(json, ResumptionToken.class);
            return tokenObject;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public String toString() {
        return gson.toJson(this);
    }

    public static void main(String[] args) {
//        String token = toToken(0L, "2000-01-01", "2020-01-01", null, "oai_dc");
//        System.out.println(token);
        ResumptionToken tokenObject = fromToken("eyJ0eG5JZCI6MTI2MDMyNiwiZnJvbSI6IjAwMDAtMDEtMDEiLCJ1bnRpbCI6Ijk5OTktMTItMzEiLCJtZXRhZGF0YVByZWZpeCI6Im9haV9kYyJ9");
        System.out.println(tokenObject);
//        tokenObject = fromToken("eyJmcm9tIjoiMDAwMC0wMS0wMSIsInVudGlsIjoiOTk5OS0xMi0zMSIsInNraXAiOjIsIm1ldGFkYXRhUHJlZml4Ijoib2FpX2RjIn0");
//        System.out.println(tokenObject);
    }
}
