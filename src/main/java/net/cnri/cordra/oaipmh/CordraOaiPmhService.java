package net.cnri.cordra.oaipmh;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import net.cnri.cordra.api.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.time.Instant;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class CordraOaiPmhService {

    private static Logger logger = LoggerFactory.getLogger(CordraOaiPmhService.class);


    private static final String BAD_ARGUMENT = "<error code='badArgument'>The request includes illegal arguments or is missing required arguments.</error>";
    private static final String BAD_VERB = "<error code='badVerb'>Value of the verb argument is not a legal OAI-PMH verb, the verb argument is missing, or the verb argument is repeated.</error>";
    private static final String ID_DOES_NOT_EXIST = "<error code='idDoesNotExist'>The value of the identifier argument is unknown or illegal in this repository.</error>";
    private static final String BAD_RESUMPTION_TOKEN = "<error code='badResumptionToken'>The value of the resumptionToken argument is invalid or expired.</error>";
    private static final String CANNOT_DISSEMINATE_FORMAT = "<error code='cannotDisseminateFormat'>The value of the metadataPrefix argument is not supported by the item identified by the value of the identifier argument.</error>";
    private static final String NO_SET_HIERARCHY = "<error code='noSetHierarchy'>The repository does not support sets.</error>";
    private static final String NO_RECORDS_MATCH = "<error code='noRecordsMatch'>The combination of the values of the from, until, set and metadataPrefix arguments results in an empty list.</error>";
    private static final String NO_METADATA_FORMATS = "<error code='noMetadataFormats'>There are no metadata formats available for the specified item.</error>";

    private static DateTimeFormatter responseDateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'").withZone(ZoneOffset.UTC);
    private OaiPmhIdentity identity;
    private int maxPageSize;
    private List<String> cordraTypes;
    private CordraClient cordra;
    private DublinCoreMetadataGenerator dcGenerator;
    private Gson gson = new Gson();

    public CordraOaiPmhService(OaiPmhConfig config) {
        this.identity = config.identity;
        this.maxPageSize = config.maxPageSize;
        try {
            cordra = new TokenUsingHttpCordraClient(config.cordraBaseUri, null, null);
        } catch (CordraException e) {
            throw new IllegalArgumentException("Could not construct CordraClient");
        }
        this.cordraTypes = config.cordraTypes;
        dcGenerator = new DublinCoreMetadataGenerator();
    }

    public String processRequest(OaiPmhRequest request) throws CordraException {
        String responseXml;
        if ("GetRecord".equals(request.verb)) {
            responseXml = getRecord(request);
        } else if ("ListRecords".equals(request.verb)) {
            responseXml = listItems(request);
        } else if ("ListIdentifiers".equals(request.verb)) {
            responseXml = listItems(request);
        } else if ("ListSets".equals(request.verb)) {
            responseXml = listSets(request);
        } else if ("Identify".equals(request.verb)) {
            responseXml = identify(request);
        } else if ("ListMetadataFormats".equals(request.verb)) {
            responseXml = listMetadataFormats(request);
        } else {
            responseXml = BAD_VERB;
        }
        return wrap(responseXml, request);
    }

    private SearchResults<CordraObject> getObjects(Long txnId, String from, String until, String set, long pageSize) throws ParseException, CordraException {
        String query = typeQueryFor(cordraTypes);
        if (from != null || until != null) {
            query += rangeQueryFor(from, until);
        }
        if (txnId != null) {
            query += rangeQueryForTxnId(txnId);
        }
        List<SortField> sortFields = new ArrayList<>();
        sortFields.add(new SortField("txnId", false));
        QueryParams queryParams = new QueryParams(0, (int) pageSize, sortFields);
        return cordra.search(query, queryParams);
    }

    public String getRecord(OaiPmhRequest request) throws CordraException {
        if (!isValidGetRecordRequest(request)) {
            return BAD_ARGUMENT;
        }
        try {
            CordraObject co = cordra.get(request.identifier);
            if (co == null) {
                return ID_DOES_NOT_EXIST;
            }
            String headerXml = cordraObjectToHeaderXml(co);
            String metadataXml = getMetadataFrom(co, cordra, request.metadataPrefix);
            if (metadataXml == null) {
                return CANNOT_DISSEMINATE_FORMAT;
            }
            StringBuilder sb = new StringBuilder();
            sb.append("<GetRecord>");
            sb.append("<record>");
            sb.append(headerXml);
            sb.append(metadataXml);
            sb.append("</record>");
            sb.append("</GetRecord>");
            return sb.toString();
        } catch (Exception ex) {
            throw new CordraException(ex);
        }
    }

    private String getMetadataFrom(CordraObject co, CordraClient cordra, String metadataPrefix) {
        String metadataXml = null;
        try {
            List<String> methods = cordra.listMethods(co.id);
            if ("oai_dc".equals(metadataPrefix) && methods.contains("getAsDublinCoreJson")) {
                metadataXml = dcGenerator.generateMetadataFrom(co, cordra);
            } else {
                Options options = new Options().setUseDefaultCredentials(true);
                JsonObject params = new JsonObject();
                params.addProperty("format", metadataPrefix);
                metadataXml = cordra.call(co.id, "getAsXml", params, options).getAsString();
            }
        } catch (CordraException e) {
            e.printStackTrace();
        }
        return metadataXml;
    }

    public String listItems(OaiPmhRequest request) throws CordraException {
        if (!isValidListRecordOrIdentifiersRequest(request)) {
            return BAD_ARGUMENT;
        }
        Long txnId = null;
        if (request.resumptionToken != null) {
            ResumptionToken resumptionToken = ResumptionToken.fromToken(request.resumptionToken);
            if (!isValidResumptionToken(resumptionToken)) {
                return BAD_RESUMPTION_TOKEN;
            }
            setResumptionTokenValuesIntoRequest(resumptionToken, request);
            if (resumptionToken.txnId != null) {
                txnId = resumptionToken.txnId+1;
            }
        }
        try {
            String from = request.from;
            String until = request.until;
            String set = request.set;
            String metadataPrefix = request.metadataPrefix;
            long pageSize = maxPageSize;
            boolean isHeadersOnly = "ListIdentifiers".equals(request.verb);
            StringBuilder sb = new StringBuilder();
            Long lastSeenTxnId = null;
            boolean hasSeenAllResults = false;
            try (SearchResults<CordraObject> objects = getObjects(txnId, from, until, set, pageSize)) {
                if (objects.size() == 0) {
                    return NO_RECORDS_MATCH;
                }
                hasSeenAllResults = (pageSize <= objects.size());
                sb.append("<").append(request.verb).append(">");
                for (CordraObject co : objects) {
                    String metadataXml = null;
                    if (!isHeadersOnly) {
                        metadataXml = getMetadataFrom(co, cordra, request.metadataPrefix);
                        if (metadataXml == null) {
                            continue;
                        }
                        sb.append("<record>");
                    }
                    String headerXml = cordraObjectToHeaderXml(co);
                    sb.append(headerXml);
                    if (!isHeadersOnly) {
                        if (metadataXml == null) {
                            continue;
                        }
                        sb.append(metadataXml);
                    }
                    if (!isHeadersOnly) {
                        sb.append("</record>");
                    }
                    lastSeenTxnId = co.metadata.txnId;
                }
            }
            if (hasSeenAllResults) {
                sb.append(buildResumptionTokenElement(lastSeenTxnId, from, until, set, metadataPrefix));
            }
            sb.append("</").append(request.verb).append(">");
            return sb.toString();
        } catch (Exception e) {
            throw new CordraException(e);
        }
    }

    private void setResumptionTokenValuesIntoRequest(ResumptionToken resumptionToken, OaiPmhRequest request) {
        request.from = resumptionToken.from;
        request.until = resumptionToken.until;
        request.metadataPrefix = resumptionToken.metadataPrefix;
        request.set = resumptionToken.set;
    }

    private String buildResumptionTokenElement(long skip, String from, String until, String set, String metadataPrefix) {
        StringBuilder sb = new StringBuilder();
        sb.append("<resumptionToken>");
        String resumptionToken = ResumptionToken.toToken(skip, from, until, set, metadataPrefix);
        sb.append(resumptionToken);
        sb.append("</resumptionToken>");
        return sb.toString();
    }

    public String listSets(OaiPmhRequest request) throws CordraException {
        return NO_SET_HIERARCHY;
    }

    public String identify(OaiPmhRequest request) throws CordraException {
        StringBuilder sb = new StringBuilder();
        sb.append("<Identify>");
        sb.append("<repositoryName>").append(identity.repositoryName).append("</repositoryName>");
        sb.append("<baseURL>").append(identity.baseURL).append("</baseURL>");
        sb.append("<protocolVersion>").append(identity.protocolVersion).append("</protocolVersion>");
        sb.append("<adminEmail>").append(identity.adminEmail).append("</adminEmail>");
        sb.append("<earliestDatestamp>").append(identity.earliestDatestamp).append("</earliestDatestamp>");
        sb.append("<deletedRecord>").append(identity.deletedRecord).append("</deletedRecord>");
        sb.append("<granularity>").append(identity.granularity).append("</granularity>");
        sb.append("</Identify>");
        return sb.toString();
    }

    public String listMetadataFormats(OaiPmhRequest request) throws CordraException {
        CordraObject co = null;
        List<MetadataFormat> formats = new ArrayList<>();
        if (request.identifier != null) {
            co = cordra.get(request.identifier);
            if (co == null) {
                return ID_DOES_NOT_EXIST;
            }
            formats = listMetadataFormatsFor(co.type);
        } else {
            formats = listMetadataFormatsFor(cordraTypes);
        }

        String xml = metadataFormatsToXml(formats);
        return xml;
    }

    private String metadataFormatsToXml(List<MetadataFormat> formats) {
        StringBuilder sb = new StringBuilder();
        sb.append("<ListMetadataFormats>");
        for (MetadataFormat format : formats) {
            sb.append("<metadataFormat>");
            if (format.metadataPrefix != null) {
                sb.append("<metadataPrefix>"+format.metadataPrefix+"</metadataPrefix>");
            }
            if (format.schema != null) {
                sb.append("<schema>"+format.schema+"</schema>");
            }
            if (format.metadataNamespace != null) {
                sb.append("<metadataNamespace>"+format.metadataNamespace+"</metadataNamespace>");
            }
            sb.append("</metadataFormat>");
        }
        sb.append("</ListMetadataFormats>");
        return sb.toString();
    }

    private static class MetadataFormat {
        public String metadataPrefix;
        public String schema;
        public String metadataNamespace;
    }

    private List<MetadataFormat> listMetadataFormatsFor(List<String> types) {
        Map<String, MetadataFormat> formatsMap = new HashMap<>();
        for (String type : types) {
            List<MetadataFormat> formatsForType = listMetadataFormatsFor(type);
            for (MetadataFormat format : formatsForType) {
                formatsMap.put(format.metadataPrefix, format);
            }
        }
        List<MetadataFormat> formats = new ArrayList<>(formatsMap.values());
        return formats;
    }

    private List<MetadataFormat> listMetadataFormatsFor(String type) {
        try {
            Options options = new Options().setUseDefaultCredentials(true);
            JsonObject params = new JsonObject();
            JsonElement result = cordra.callForType(type, "listMetadataFormats", params, options);
            List<MetadataFormat> formats = gson.fromJson(result, new TypeToken<List<MetadataFormat>>() {}.getType());
            return formats;
        } catch (CordraException e) {
            logger.error("Could not list metadata formats for type " + type + ". " + e.getMessage(), e);
            return Collections.EMPTY_LIST;
        }
    }

    private boolean isValidGetRecordRequest(OaiPmhRequest request) {
        if (request.identifier == null) {
            logger.info("Bad GetRecord request identifier is missing");
            return false;
        }
        if (request.metadataPrefix == null) {
            logger.info("Bad GetRecord request metadataPrefix is missing");
            return false;
        }
        return true;
    }

    private boolean isValidListRecordOrIdentifiersRequest(OaiPmhRequest request) {
        if (request.resumptionToken != null) {
            if (request.from != null || request.until != null || request.set != null || request.metadataPrefix != null) {
                logger.info("Bad List request, incorrect additional params with resumptionToken");
                return false;
            } else {
                return true;
            }
        }
        if (request.metadataPrefix == null) {
            logger.info("Bad List request metadataPrefix is missing");
            return false;
        }
        if (request.from != null) {
            if (!isValidTimeStamp(request.from)) {
                logger.info("Bad List request from is invalid: " + request.from);
                return false;
            }
        }
        if (request.until != null) {
            if (!isValidTimeStamp(request.until)) {
                logger.info("Bad List request until is invalid: " + request.until);
                return false;
            }
        }
        if (request.set != null) {
            //Sets are not currently supported
        }
        return true;
    }

    private static boolean isValidTimeStamp(String timestamp) {
        try {
            Instant i = ZonedDateTime.parse(timestamp, dateTimeFormat).toInstant();
            //The legitimate formats are YYYY-MM-DD and YYYY-MM-DDThh:mm:ssZ
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    private boolean isValidResumptionToken(ResumptionToken resumptionToken) {
        if (resumptionToken == null) {
            logger.info("Invalid resumptionToken. Token is null");
            return false;
        }
        if (resumptionToken.from != null) {
            if (!isValidTimeStamp(resumptionToken.from)) {
                logger.info("Invalid resumptionToken. resumptionToken.from is invalid: " + resumptionToken.from);
                return false;
            }
        }
        if (resumptionToken.until != null) {
            if (!isValidTimeStamp(resumptionToken.until)) {
                logger.info("Invalid resumptionToken. resumptionToken.until is invalid: " + resumptionToken.until);
                return false;
            }
        }
        if (resumptionToken.txnId != null) {
            if (resumptionToken.txnId < 0) {
                logger.info("Invalid resumptionToken. resumptionToken.txnId is less than 0: " + resumptionToken.txnId);
                return false;
            }
        }
        return true;
    }

    private String wrap(String responseXml, OaiPmhRequest request) {
        StringBuilder sb = new StringBuilder();
        sb.append("<?xml version='1.0' encoding='UTF-8'?>");
        sb.append("<OAI-PMH xmlns='http://www.openarchives.org/OAI/2.0/' " +
                "xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' " +
                "xsi:schemaLocation='http://www.openarchives.org/OAI/2.0/ http://www.openarchives.org/OAI/2.0/OAI-PMH.xsd'>");
        sb.append(buildTimeStampElement());
        sb.append(buildRequestElement(request));
        sb.append(responseXml);
        sb.append("</OAI-PMH>");
        return sb.toString();
    }

    private String buildTimeStampElement() {
        StringBuilder sb = new StringBuilder();
        Instant now = Instant.now();
        sb.append("<responseDate>");
        sb.append(responseDateFormatter.format(now));
        sb.append("</responseDate>");
        return sb.toString();
    }

    private String buildRequestElement(OaiPmhRequest request) {
        StringBuilder sb = new StringBuilder();
        sb.append("<request");
        if (request.resumptionToken != null) {
            sb.append(" resumptionToken='" + request.resumptionToken + "'");
        }
        if (request.metadataPrefix != null) {
            sb.append(" metadataPrefix='" + request.metadataPrefix + "'");
        }
        if (request.verb != null) {
            sb.append(" verb='" + request.verb + "'");
        }
        if (request.from != null) {
            sb.append(" from='" + request.from + "'");
        }
        if (request.until != null) {
            sb.append(" until='" + request.until + "'");
        }
        if (request.identifier != null) {
            sb.append(" identifier='" + request.identifier + "'");
        }
        if (request.set != null) {
            sb.append(" set='" + request.set + "'");
        }
        sb.append(">");
        sb.append(identity.baseURL);
        sb.append("</request>");
        return sb.toString();
    }

    private String cordraObjectToHeaderXml(CordraObject co) {
        String identifier = co.id;
        Instant modifiedOn = Instant.ofEpochMilli(co.metadata.modifiedOn);
        String datestamp = responseDateFormatter.format(modifiedOn);;
        StringBuilder xml = new StringBuilder();
        xml.append("<header>");
        xml.append("<identifier>").append(identifier).append("</identifier>");
        xml.append("<datestamp>").append(datestamp).append("</datestamp>");
        xml.append("</header>");
        return xml.toString();
    }

    //private static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
    private static DateTimeFormatter dateTimeFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ssX").withZone(ZoneOffset.UTC);
    private static DateTimeFormatter indexedDateFormatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmssSSS").withZone(ZoneOffset.UTC);

    private static String rangeQueryFor(String from, String until) throws ParseException {
        long fromMs = 0L;
        long untilMs = 0L;
        if (from != null) {
            fromMs = msFromDateTimeString(from);
        }
        if (until != null) {
            untilMs = msFromDateTimeString(until);
            untilMs += 999L;
        } else {
            untilMs = msFromDateTimeString("9999-12-31T00:00:00Z");
        }
        String fromTimeString = getAsIndexString(fromMs);
        String untilTimeString = getAsIndexString(untilMs);
        String query = " +objmodified:[" + fromTimeString + " TO " + untilTimeString + "]";
        return query;
    }

    private String rangeQueryForTxnId(Long txnId) {
        String query = " +txnId:[" + txnId + " TO " + Long.MAX_VALUE + "]";
        return query;
    }

    private static long msFromDateTimeString(String s) {
        Instant i = ZonedDateTime.parse(s, dateTimeFormat).toInstant();
        long ms = i.toEpochMilli();
        return ms;
    }

    private String typeQueryFor(List<String> types) {
        StringBuilder query = new StringBuilder();
        query.append("+(");
        for (String type : types) {
            query.append(" type:").append(type);
        }
        query.append(")");
        return query.toString();
    }

    private static String getAsIndexString(long timestamp) {
        Instant i = Instant.ofEpochMilli(timestamp);
        return indexedDateFormatter.format(i);
    }
}
