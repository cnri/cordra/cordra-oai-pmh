package net.cnri.cordra.oaipmh;

public class OaiPmhIdentity {
    public String repositoryName = "Example Name";
    public String baseURL = "http://example.com/cordra-oai";
    public final String protocolVersion = "2.0";
    public String earliestDatestamp = "1900-01-01T00:00:00Z";
    public final String deletedRecord = "no";
    public final String granularity = "YYYY-MM-DDThh:mm:ssZ"; //YYYY-MM-DDThh:mm:ssZ or YYYY-MM-DD
    public String adminEmail = "example@example.com";
}
