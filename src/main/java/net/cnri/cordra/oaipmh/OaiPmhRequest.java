package net.cnri.cordra.oaipmh;

import javax.servlet.http.HttpServletRequest;

public class OaiPmhRequest {

    public String verb;
    public String metadataPrefix;
    public String identifier;
    public String from;
    public String until;
    public String set;
    public String resumptionToken;

    public OaiPmhRequest(HttpServletRequest request) {
        this.verb = request.getParameter("verb");
        this.metadataPrefix = request.getParameter("metadataPrefix");
        this.identifier = request.getParameter("identifier");
        this.from = request.getParameter("from");
        this.until = request.getParameter("until");
        this.set = request.getParameter("set");
        this.resumptionToken = request.getParameter("resumptionToken");
    }
}
